FROM python:3.12.1-slim-bullseye

ENV PYTHONUNBUFFERED=1
ENV PYTHONOPTIMIZE=1
ENV POETRY="/root/.local/bin/poetry"

WORKDIR /app
COPY pyproject.toml poetry.lock scripts/entrypoint.sh scripts/create_tables.py /app/
RUN chmod +x entrypoint.sh
RUN apt-get update  \
 && apt-get --assume-yes install gcc curl \
 && curl -sSL https://install.python-poetry.org | python3 - --version 1.4.2 \
 && $POETRY --version \
 && $POETRY config virtualenvs.create false \
 && $POETRY install --without dev --no-interaction
COPY src/adtech_trial adtech_trial

EXPOSE 8000

ENTRYPOINT ["./entrypoint.sh"]
