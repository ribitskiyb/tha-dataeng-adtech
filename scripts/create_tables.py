"""
This script is meant to be run from the app container as a part of a local installation of the app
"""
from sqlalchemy import create_engine

from adtech_trial._app.dependencies import Settings
from adtech_trial._app.models import Base

settings = Settings()
engine = create_engine(url=settings.pg_dsn)
Base.metadata.create_all(bind=engine)
