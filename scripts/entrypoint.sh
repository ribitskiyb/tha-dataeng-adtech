#!/usr/bin/env bash
set -euo pipefail

if [[ "$1" == "--create-tables" ]]; then
  python3 create_tables.py
fi

gunicorn \
  --bind=0.0.0.0:8000 \
  --workers=4 \
  --worker-class uvicorn.workers.UvicornWorker \
  adtech_trial.main:app
