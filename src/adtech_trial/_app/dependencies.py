from typing import Type

from pydantic import Field, PostgresDsn
from pydantic_settings import BaseSettings
from sqlalchemy import URL, create_engine
from sqlalchemy.orm import Session, sessionmaker


class container:
    """Just a namespace for dependencies initialized at application start time

    A "dependency" is anything that requires interacting with application environment (usually via
    env variables) to instantiate itself.
    """

    DbSession: Type[Session] = None  # type: ignore


class Settings(BaseSettings):
    pg_username: str = Field(alias="ADT_POSTGRES_USER")
    pg_password: str = Field(alias="ADT_POSTGRES_PASSWORD")
    pg_host: str = Field(alias="ADT_POSTGRES_HOST")
    pg_port: int = Field(default=5432, alias="ADT_POSTGRES_PORT")
    pg_database: str = Field(alias="ADT_POSTGRES_DB")

    @property
    def pg_dsn(self) -> URL:
        return URL.create(
            drivername="postgresql",
            username=self.pg_username,
            password=self.pg_password,
            host=self.pg_host,
            port=self.pg_port,
            database=self.pg_database,
        )


def get_bound_db_session_class(settings: Settings) -> Type[Session]:
    """Initializes a SQLAlchemy DB engine and returns a Session class bound to it"""
    engine = create_engine(settings.pg_dsn)
    SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

    return SessionLocal  # type: ignore
