import itertools
from datetime import datetime
from typing import AbstractSet, Any, Iterator, Mapping, Optional, Sequence

from pydantic.config import JsonDict
from sqlalchemy import TextClause, func, text

from adtech_trial._app.models import Event
from adtech_trial._app.schemas import (
    EventAttribute,
    EventCreate,
    EventMetric,
    FilterItem,
    Granularity,
)
from adtech_trial._app.utils import assert_never, inverse_mapping

# NOTE: Given the current response object structure, a list of objects like this one:
# {
#     "attribute1": 123456,
#     "attribute2": 123456,
#     "attribute3": 123456,
#     "attribute4": "Lorem ipsum dolor sit amet",
#     "attribute5": "Lorem ipsum dolor sit amet",
#     "attribute6": True,
#     "date": "2024-01-01T09:00:00",
#     "metric1": 1234,
#     "metric2": 1234.56,
# }
# ... the 40k rows threshold leads to a max size of JSON reponse body of ~11Mb, which is just a nice
# arbitrary number to stick to.
# In a real-world scenario the threshold should be estimated based on multiple factors like expected
# RPS to the service, ratio of create-event vs get-analytics requests, etc.
ANALYTICS_RESULT_ROWS_THRESHOLD = 40_000

RESPONSE_ITEM_DATE_KEY = "date"
CURRENCY_TO_INT_COEF = 100  # assuming the smallest currency unit is a cent

_TOTAL_ROWCOUNT_COLUMN = "_total_rows"
# noinspection PyUnresolvedReferences
# These mappings decouple API design from DB schema design, allowing them to change independently
_METRIC_API_TO_DB_NAME: Mapping[EventMetric, str] = {
    "metric1": Event.metric1.name,
    "metric2": Event.metric2.name,
}
# noinspection PyUnresolvedReferences
_ATTRIBUTE_API_TO_DB_NAME: Mapping[EventAttribute, str] = {
    "attribute1": Event.attribute1.name,
    "attribute2": Event.attribute2.name,
    "attribute3": Event.attribute3.name,
    "attribute4": Event.attribute4.name,
    "attribute5": Event.attribute5.name,
    "attribute6": Event.attribute6.name,
}
_METRIC_DB_TO_API_NAME: Mapping[str, EventMetric] = inverse_mapping(_METRIC_API_TO_DB_NAME)
_ATTRIBUTE_DB_TO_API_NAME: Mapping[str, EventAttribute] = inverse_mapping(_ATTRIBUTE_API_TO_DB_NAME)

# noinspection PyUnresolvedReferences
_METRIC_AGG_EXPR: Mapping[str, str] = {
    (m := Event.metric1.name): f"sum({m})",
    (m := Event.metric2.name): f"sum({m})::float / {CURRENCY_TO_INT_COEF}",
}


def get_event_model(event: EventCreate) -> Event:
    # noinspection PyArgumentList
    db_event = Event(
        external_id=event.id,
        date=func.date(event.event_date),
        datetime=event.event_date,
        attribute1=event.attribute1,
        attribute2=event.attribute2,
        attribute3=event.attribute3,
        attribute4=event.attribute4,
        attribute5=event.attribute5,
        attribute6=event.attribute6,
        metric1=event.metric1,
        metric2=int(event.metric2 * CURRENCY_TO_INT_COEF),
    )

    return db_event


def get_analytics_query_statement(
    granularity: Granularity,
    metrics: AbstractSet[EventMetric],
    group_by: AbstractSet[EventAttribute] = frozenset(),
    attr_filters: Sequence[FilterItem] = (),
    start_ts: Optional[datetime] = None,
    end_ts: Optional[datetime] = None,
) -> TextClause:
    """Renders executable SQLAlchemy analytics query object from the API input. Sanitizes user input
    and guarantees that of API- and DB-names of metrics and attributes are consistent.
    """
    if not metrics:
        raise ValueError("at least one metric must be provided")

    date_col = Event.date.name  # type: ignore
    datetime_col = Event.datetime.name  # type: ignore
    events_table = Event.__tablename__
    metric_columns = [_METRIC_API_TO_DB_NAME[m] for m in metrics]  # noqa
    group_by_columns = [_ATTRIBUTE_API_TO_DB_NAME[a] for a in group_by]
    attr_column_filters = {
        _ATTRIBUTE_API_TO_DB_NAME[item.attribute]: item.value_as_intended_type
        for item in attr_filters
    }

    attr_conditions = " AND ".join(f"{attr} = :{attr}" for attr in attr_column_filters)
    and_attr_conditions = f"AND {attr_conditions}" if attr_conditions else ""
    time_attrs_metrics = ", ".join(
        (
            (
                datetime_col
                if granularity == "hourly"
                else date_col
                if granularity == "daily"
                else assert_never()
            ),
            *group_by_columns,
            *metric_columns,
        )
    )
    if not (start_ts or end_ts or attr_column_filters):
        subquery_w_params = f"SELECT * FROM {events_table}"
    elif attr_column_filters and not (start_ts or end_ts):
        subquery_w_params = f"SELECT * FROM {events_table} WHERE {attr_conditions}"
    elif start_ts and not end_ts:
        subquery_w_params = f"""
            SELECT {time_attrs_metrics}
            FROM {events_table}
            WHERE {date_col} = :start_date AND {datetime_col} >= :start_ts
                  {and_attr_conditions}
            UNION ALL
            SELECT {time_attrs_metrics}
            FROM {events_table}
            WHERE {date_col} > :start_date
                  {and_attr_conditions}
        """
    elif not start_ts and end_ts:
        subquery_w_params = f"""
            SELECT {time_attrs_metrics}
            FROM {events_table}
            WHERE {date_col} < :end_date
                  {and_attr_conditions}
            UNION ALL
            SELECT {time_attrs_metrics}
            FROM {events_table}
            WHERE {date_col} = :end_date AND {datetime_col} <= :end_ts
                  {and_attr_conditions}
        """
    else:
        subquery_w_params = f"""
            SELECT {time_attrs_metrics}
            FROM {events_table}
            WHERE {date_col} = :start_date AND {datetime_col} >= :start_ts
                  {and_attr_conditions}
            UNION ALL
            SELECT {time_attrs_metrics}
            FROM {events_table}
            WHERE {date_col} > :start_date AND {date_col} < :end_date
                  {and_attr_conditions}
            UNION ALL
            SELECT {time_attrs_metrics}
            FROM {events_table}
            WHERE {date_col} = :end_date AND {datetime_col} <= :end_ts
                  {and_attr_conditions}
        """

    # A minor optimization: converting timestamp to string right away in the format required by the
    # API contract, so that we don't have to do this on the server worker
    datetime_granule = "to_char({}, 'YYYY-MM-DD\"T\"HH24:MI:SS')".format(
        f"date_trunc('hour', {datetime_col})"
        if granularity == "hourly"
        else date_col
        if granularity == "daily"
        else assert_never()
    )
    if group_by_columns:
        grouping_attrs_w_aliases = "," + ", ".join(
            f"{col} AS {_ATTRIBUTE_DB_TO_API_NAME[col]}" for col in group_by_columns
        )
        grouping_attrs = "," + ", ".join(group_by_columns)
    else:
        grouping_attrs_w_aliases = ""
        grouping_attrs = ""
    metric_agg_exprs = "," + ", ".join(
        f"{_METRIC_AGG_EXPR[col]} AS {_METRIC_DB_TO_API_NAME[col]}" for col in metric_columns
    )
    query_w_params = f"""
        WITH main AS (
            SELECT
                {datetime_granule} AS "{RESPONSE_ITEM_DATE_KEY}"
                {grouping_attrs_w_aliases}
                {metric_agg_exprs}
            FROM ({subquery_w_params}) _
            GROUP BY
                {datetime_granule}
                {grouping_attrs}
        )
        SELECT
             main.*
            ,total.count AS {_TOTAL_ROWCOUNT_COLUMN}
        FROM main
            ,(SELECT count(*) AS count FROM main) total
    """

    bindparam_values: dict[str, Any] = attr_column_filters
    if start_ts:
        bindparam_values.update(
            start_ts=start_ts,
            start_date=start_ts and start_ts.date(),
        )
    if end_ts:
        bindparam_values.update(
            end_ts=end_ts,
            end_date=end_ts and end_ts.date(),
        )
    query_w_bound_params = text(query_w_params).bindparams(**bindparam_values)

    return query_w_bound_params


def try_make_analytics_endpoint_response_payload(
    query_result: Iterator[Mapping[str, Any]],
    metrics: Sequence[EventMetric],
    group_by: Sequence[EventAttribute],
) -> list[JsonDict] | int:
    """
    Helper function to make a jsonable result that can be immediately returned from the endpoint.
    For performance reasons it also checks that the total amount of rows in the query result doesn't
    exceed the threshold we've established for the endpoint.

    The logic here is coupled with :py:func:`render_analytics_query` in that it expects specific
    columns to be present, including column with the total rowcount.

    :param query_result: The resulting row set of the analytics query, where each row is required to
    be represented as a mapping (column-name -> value)
    :param metrics: List of metrics from the API request
    :param group_by: List of attributes to group by from the API request
    :return: Either the payload -- a list of json objects ("rows"), or a total number of rows in the
    query result when this number exceeds the threshold.
    """
    first_row = next(query_result, None)
    if first_row is None:
        return []

    total_rows = first_row[_TOTAL_ROWCOUNT_COLUMN]
    if total_rows > ANALYTICS_RESULT_ROWS_THRESHOLD:
        return total_rows

    response_item_keys = (*group_by, RESPONSE_ITEM_DATE_KEY, *metrics)
    response_items = [
        {key: row[key] for key in response_item_keys}
        for row in itertools.chain((first_row,), query_result)
    ]

    return response_items
