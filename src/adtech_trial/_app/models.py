from sqlalchemy import BigInteger, Boolean, Column, Date, DateTime, Index, Integer, String
from sqlalchemy.orm import DeclarativeMeta, declarative_base

Base: DeclarativeMeta = declarative_base()


class Event(Base):
    __tablename__ = "events"

    id = Column(BigInteger, primary_key=True, index=True, autoincrement=True)
    external_id = Column(BigInteger, nullable=False, unique=True)
    date = Column(Date, nullable=False)
    datetime = Column(DateTime, nullable=False)
    metric1 = Column(Integer, nullable=False)
    metric2 = Column(BigInteger, nullable=False)
    attribute1 = Column(Integer, nullable=True)
    attribute2 = Column(Integer, nullable=True)
    attribute3 = Column(Integer, nullable=True)
    attribute4 = Column(String, nullable=True)
    attribute5 = Column(String, nullable=True)
    attribute6 = Column(Boolean, nullable=True)


# noinspection PyTypeChecker
Index("ix_events_date", Event.date)
