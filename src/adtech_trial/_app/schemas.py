from datetime import datetime
from typing import Callable, Literal, Mapping, Optional, TypeAlias

from pydantic import BaseModel, model_validator

EventAttribute: TypeAlias = Literal[
    "attribute1",
    "attribute2",
    "attribute3",
    "attribute4",
    "attribute5",
    "attribute6",
]

EventMetric: TypeAlias = Literal[
    "metric1",
    "metric2",
]

Granularity: TypeAlias = Literal[
    "hourly",
    "daily",
]


class EventCreate(BaseModel):
    id: int
    event_date: datetime
    metric1: int
    # NOTE: although this is a currency-type value, we still parse it as IEEE 754 float because
    # we'll store it internally as an integer (assuming 2 decimal places)
    metric2: float
    attribute1: Optional[int] = None
    attribute2: Optional[int] = None
    attribute3: Optional[int] = None
    attribute4: Optional[str] = None
    attribute5: Optional[str] = None
    attribute6: Optional[bool] = None


# `(str) -> str` instead of `(Any) -> Any` to help enforce types in `FilterItem`
def _same_str(s: str) -> str:
    return s


class FilterItem(BaseModel):
    """A single analytics query filter by some attribute's value

    This model mixes two responsibilities: to validate API endpoint's parameter as defined in the
    OpenAPI spec and to provide the value of the attribute of its intended type. Because of that
    the model validates the value as string and converts this string to the attribute's intended
    type "under the hood". This is hacky 💩, but more efficient than other options to implement the
    same idea.
    """

    attribute: EventAttribute
    value: str

    # The callable will either return a value or raise any exception
    # if attribute's string value doesn't conform to its intended type
    _str_to_intended_type: Mapping["EventAttribute", Callable[[str], int | str | bool]] = {
        "attribute1": int,
        "attribute2": int,
        "attribute3": int,
        "attribute4": _same_str,
        "attribute5": _same_str,
        "attribute6": {"true": True, "false": False}.__getitem__,
    }

    @model_validator(mode="after")
    def _enforce_attribute_type(self) -> "FilterItem":
        # `__getitem__` instead of `get` to avoid accidentally storing some attribute as string when
        # its intended type was something else and fail fast
        get_intended_type_value = self._str_to_intended_type[self.attribute]
        try:
            self._value_as_intended_type = get_intended_type_value(self.value)  # noqa
            return self
        except Exception:  # noqa
            pass

        raise ValueError(f"{self.value!r} is not a valid value of attribute {self.attribute!r}")

    @property
    def value_as_intended_type(self) -> int | str | bool:
        return self._value_as_intended_type
