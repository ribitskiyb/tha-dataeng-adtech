from datetime import datetime
from typing import Annotated, Optional, cast

from fastapi import Depends, FastAPI, Query, Request, status
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import HTTPException, RequestValidationError
from pydantic import StringConstraints, ValidationError
from pydantic_core import PydanticUndefinedType
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session
from starlette.responses import JSONResponse

from adtech_trial._app.dependencies import container as dc
from adtech_trial._app.logic import (
    ANALYTICS_RESULT_ROWS_THRESHOLD,
    get_analytics_query_statement,
    get_event_model,
    try_make_analytics_endpoint_response_payload,
)
from adtech_trial._app.schemas import (
    EventAttribute,
    EventCreate,
    EventMetric,
    FilterItem,
    Granularity,
)

app = FastAPI()


def get_db_session():
    session = dc.DbSession()
    try:
        yield session
    finally:
        session.close()


@app.exception_handler(RequestValidationError)
def handle_validation_error_as_405(_: Request, exc: RequestValidationError):
    # Normally pydantic should always provide error details in this attribute's tuple
    error_details = exc.args[0] if exc.args else "unknown"
    return JSONResponse(
        status_code=status.HTTP_405_METHOD_NOT_ALLOWED,
        content={
            "detail": jsonable_encoder(
                error_details,
                custom_encoder={
                    # Workaround for a bug in FastAPI
                    # https://github.com/tiangolo/fastapi/issues/9920
                    PydanticUndefinedType: lambda _: None,
                },
            )
        },
    )


@app.post("/event", response_model=EventCreate, status_code=status.HTTP_200_OK)
def create_event(event: EventCreate, db: Session = Depends(get_db_session)):
    db_event = get_event_model(event)
    db.add(db_event)
    try:
        db.commit()
    except IntegrityError:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail=f"Event with id={event.id} already exists."
        )

    return event


@app.get("/analytics/query")
def get_analytics(
    group_by: list[EventAttribute] = Query(
        ...,
        description="Attributes for grouping",
        alias="groupBy",
    ),
    metrics: list[EventMetric] = Query(
        ...,
        description="Metrics to retrieve (always sums)",
        min_length=1,
    ),
    granularity: Granularity = Query(..., description="Granularity (hourly or daily)"),
    filters: Optional[
        list[
            Annotated[
                str, StringConstraints(pattern=r"(?P<attribute_name>\w+):(?P<attribute_value>\w+)")
            ]
        ]
    ] = Query(
        None,
        description='Attribute-value pair as a "{key}:{value}" string (supports multiple inputs)',
        min_length=1,
    ),
    start_ts: Optional[datetime] = Query(
        None,
        description="Start date and time for filtering (format: YYYY-MM-DDTHH:mm:ss)",
        alias="startDate",
    ),
    end_ts: Optional[datetime] = Query(
        None,
        description="End date and time for filtering (format: YYYY-MM-DDTHH:mm:ss)",
        alias="endDate",
    ),
    db: Session = Depends(get_db_session),
):
    filters = filters or ()  # type: ignore[assignment]
    try:
        filter_items = [
            FilterItem(attribute=cast(EventAttribute, name), value=value)
            for name, _, value in (item.partition(":") for item in filters)  # type: ignore
        ]
    except ValidationError as err:
        info, *_ = err.errors()
        error_msg = (
            f"Invalid filter item: {info['msg']} [type={info['type']}, input_value={info['input']}]"
        )
        raise HTTPException(status_code=status.HTTP_405_METHOD_NOT_ALLOWED, detail=error_msg)

    query = get_analytics_query_statement(
        group_by=set(group_by),
        metrics=set(metrics),
        granularity=granularity,
        attr_filters=filter_items,
        start_ts=start_ts,
        end_ts=end_ts,
    )
    with db.get_bind().connect() as conn:
        result_as_maps = (
            conn.execution_options(
                stream_results=True,
                max_row_buffer=1000,
            )
            .execute(query)
            .mappings()
        )
        payload = try_make_analytics_endpoint_response_payload(
            query_result=result_as_maps,
            metrics=metrics,
            group_by=group_by,
        )
    if isinstance(payload, int):
        total_rowcount = payload
        raise HTTPException(
            # https://www.rfc-editor.org/rfc/rfc9110.html#name-403-forbidden
            # "... the server understood the request but refuses to fulfill it"
            status_code=status.HTTP_403_FORBIDDEN,
            detail=f"Your request would produce {total_rowcount} items in the response,"
            f" while the service is able to process {ANALYTICS_RESULT_ROWS_THRESHOLD} max.",
        )

    return payload
