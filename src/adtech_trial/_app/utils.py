from typing import Hashable, Mapping, NoReturn, TypeVar

_K = TypeVar("_K", bound=Hashable)
_THashable = TypeVar("_THashable", bound=Hashable)


def assert_never() -> NoReturn:
    """Typing helper to test for exhaustiveness in match/if statements and if-expressions"""
    raise AssertionError


def inverse_mapping(mapp: Mapping[_K, _THashable]) -> dict[_THashable, _K]:
    return {v: k for k, v in mapp.items()}
