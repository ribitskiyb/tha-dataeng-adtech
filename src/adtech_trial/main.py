from ._app.dependencies import Settings
from ._app.dependencies import container as di_container
from ._app.dependencies import get_bound_db_session_class
from ._app.server import app

__all__ = ["app"]

settings = Settings()  # reading env variables happens here

di_container.DbSession = get_bound_db_session_class(settings)
