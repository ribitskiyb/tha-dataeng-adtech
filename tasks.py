import sys
from pathlib import Path

import uvicorn
from dotenv import load_dotenv
from invoke import task
from sqlalchemy import create_engine, text

from adtech_trial._app.dependencies import Settings
from adtech_trial._app.models import Base

REPO_ROOT = Path(__file__).parent
CONFIGS_DIR = REPO_ROOT / "config"
DB_CONTAINER_NAME = "adt_db"


@task
def run_pg(ctx):
    """Runs a Postgres container using test config"""
    settings = _get_settings_for_env("test")

    ctx.run(
        command=(
            f"docker run"
            f" --name {DB_CONTAINER_NAME}"
            f" -e POSTGRES_USER={settings.pg_username}"
            f" -e POSTGRES_PASSWORD={settings.pg_password}"
            f" -p {settings.pg_port}:5432"
            f" -d"
            f" postgres:16.2"
        ),
        echo=True,
    )
    ctx.run(f"docker ps --filter name={DB_CONTAINER_NAME}")


@task
def setup_db(_ctx, env, recreate=False):
    """Creates a "database" on a fresh Postgres instance and tables in it"""
    settings = _get_settings_for_env(env)

    drop_db = text(f"DROP DATABASE IF EXISTS {settings.pg_database};")
    create_db = text(
        f"CREATE DATABASE {settings.pg_database}"
        f" ENCODING 'UTF8'"
        f" LC_COLLATE = 'en_US.utf8'"
        f" LC_CTYPE = 'en_US.utf8'"
        f";"
    )

    with create_engine(
        url=settings.pg_dsn.set(database="postgres"),
    ).connect().execution_options(
        # Despite the name this disables running the command inside a transaction
        isolation_level="AUTOCOMMIT",
    ) as conn:
        if recreate:
            conn.execute(drop_db)
        conn.execute(create_db)

    Base.metadata.create_all(bind=create_engine(settings.pg_dsn))


@task
def debug(_ctx, env):
    """Runs the app locally for manual testing and debug"""
    _load_env_vars(env)
    from adtech_trial.main import app

    uvicorn.run(app, host="0.0.0.0", port=8000)


@task
def compose_up(ctx):
    """Launches the app stack with Docker Compose"""
    env_file = _load_env_vars("local")
    settings = Settings()
    ctx.run(
        f"POSTGRES_DB={settings.pg_database}"
        f" POSTGRES_USER={settings.pg_username}"
        f" POSTGRES_PASSWORD={settings.pg_password}"
        f" POSTGRES_PORT={settings.pg_port}"
        f" APP_ENV_FILE={env_file}"
        f" docker compose"
        f" up"
        f" --remove-orphans"
        f" --detach"
        f" --build"
    )


@task
def compose_down(ctx):
    """Shuts the app stack down"""
    env_file = _load_env_vars("local")
    settings = Settings()
    ctx.run(
        f"POSTGRES_CONTAINER_NAME={DB_CONTAINER_NAME}"
        f" POSTGRES_DB={settings.pg_database}"
        f" POSTGRES_USER={settings.pg_username}"
        f" POSTGRES_PASSWORD={settings.pg_password}"
        f" POSTGRES_PORT={settings.pg_port}"
        f" APP_ENV_FILE={env_file}"
        f" docker compose"
        f" down"
    )


def _load_env_vars(env_name: str) -> Path:
    env_file = CONFIGS_DIR / f"{env_name}.env"
    if not env_file.exists():
        sys.exit(f"Env-file doesn't exist: {env_file}")
    load_dotenv(env_file)
    return env_file


def _get_settings_for_env(env_name: str) -> Settings:
    _load_env_vars(env_name)
    return Settings()
