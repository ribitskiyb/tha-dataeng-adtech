from pathlib import Path
from typing import Any, Mapping, Sequence

import pytest
from dotenv import load_dotenv
from fastapi.testclient import TestClient
from sqlalchemy import TextClause, create_engine, text

REPO_ROOT = Path(__file__).parents[1]
CONFIGS_DIR = REPO_ROOT / "config"


@pytest.fixture(scope="session")
def _load_dotenv():
    load_dotenv(dotenv_path=CONFIGS_DIR / "test.env")


@pytest.fixture(scope="session")
def _app(_load_dotenv):
    from adtech_trial.main import app

    return app


@pytest.fixture(scope="session")
def _settings(_load_dotenv):
    from adtech_trial.main import settings

    return settings


@pytest.fixture(scope="session")
def engine(_settings):
    engine = create_engine(_settings.pg_dsn)
    yield engine
    engine.dispose()


@pytest.fixture(scope="session")
def client(_app):
    return TestClient(_app)


@pytest.fixture
def execute_query(engine):
    with engine.connect() as conn:

        def closure(sql: str | TextClause) -> Sequence[Mapping[str, Any]]:
            sql = sql if isinstance(sql, TextClause) else text(sql)
            return conn.execute(sql).mappings().fetchall()

        yield closure


@pytest.fixture
def execute_command(engine):
    with engine.connect() as conn:
        yield lambda sql: conn.execute(text(sql))
        conn.commit()


@pytest.fixture
def truncate_events_table(execute_command):
    yield
    execute_command("TRUNCATE TABLE events")
