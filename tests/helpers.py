import string
from datetime import datetime
from itertools import chain
from random import randint, shuffle

from pydantic.config import JsonDict

from adtech_trial._app.logic import CURRENCY_TO_INT_COEF

_ascii_lowercase_and_digits = list(chain(string.ascii_lowercase, string.digits))


def dummy_event_json(
    id: int | None = None,  # noqa
    event_date: datetime | str | None = None,
    metric1: int | None = None,
    metric2: float | None = None,
    attribute1: int | None = None,
    attribute2: int | None = None,
    attribute3: int | None = None,
    attribute4: str | None = None,
    attribute5: str | None = None,
    attribute6: bool | None = None,
) -> JsonDict:
    return dict(
        id=id or randint(0, 100_000),
        event_date=event_date
        if isinstance(event_date, str)
        else (event_date or datetime.now()).strftime("%Y-%m-%dT%H:%M:%S"),
        metric1=metric1 or randint(0, 100_000),
        metric2=metric2 or randint(0, 10_000) / CURRENCY_TO_INT_COEF,
        attribute1=attribute1,
        attribute2=attribute2,
        attribute3=attribute3,
        attribute4=attribute4,
        attribute5=attribute5,
        attribute6=attribute6,
    )


def random_ascii_alphanum(length: int = 8) -> str:
    shuffle(_ascii_lowercase_and_digits)
    return "".join(_ascii_lowercase_and_digits[:length])
