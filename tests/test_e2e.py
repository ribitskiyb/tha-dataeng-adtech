from datetime import datetime
from typing import Iterable, Tuple

import pytest
from pydantic.config import JsonDict

from tests.helpers import dummy_event_json as e


def _cases() -> Iterable[Tuple[str, JsonDict]]:
    _common_events = [
        e(10, "2000-01-01T01:00:00", 1, 0.1, 42, 42, 42, "A", "B", True),
        e(11, "2000-01-01T02:00:00", 1, 0.1, 42, 42, 42, "A", "B", True),
        e(12, "2000-01-01T03:00:00", 1, 0.1),
        e(21, "2000-01-02T01:00:00", 1, 0.2, 42, 42, 42, "A", "B", True),
        e(22, "2000-01-02T02:00:00", 1, 0.1, 42, 42, 42, "A", "B", True),
        e(23, "2000-01-02T03:00:00", 1, 0.1),
        e(31, "2000-01-03T01:00:00", 1, 0.1, 42, 42, 42, "A", "B", True),
        e(32, "2000-01-03T02:00:00", 1, 0.1, 42, 42, 42, "A", "B", True),
        e(33, "2000-01-03T03:00:00", 1, 0.1),
    ]

    all_arguments = dict(
        query=dict(
            groupBy=["attribute1", "attribute4"],
            metrics=["metric1", "metric2"],
            granularity="daily",
            filters=["attribute2:42", "attribute3:42", "attribute5:B", "attribute6:true"],
            startDate=datetime(2000, 1, 1, 1, 0, 1),
            endDate=datetime(2000, 1, 3, 1, 59, 59),
        ),
        events=_common_events,
        analytics=[
            (42, "A", "2000-01-01T00:00:00", 1, 0.1),
            (42, "A", "2000-01-02T00:00:00", 2, 0.3),
            (42, "A", "2000-01-03T00:00:00", 1, 0.1),
        ],
    )
    no_end_date = dict(
        query=dict(
            groupBy=["attribute1", "attribute4"],
            metrics=["metric1", "metric2"],
            granularity="daily",
            filters=["attribute2:42", "attribute3:42", "attribute5:B", "attribute6:true"],
            startDate=datetime(2000, 1, 1, 1, 0, 1),
        ),
        events=_common_events,
        analytics=[
            (42, "A", "2000-01-01T00:00:00", 1, 0.1),
            (42, "A", "2000-01-02T00:00:00", 2, 0.3),
            (42, "A", "2000-01-03T00:00:00", 2, 0.2),
        ],
    )
    no_start_date = dict(
        query=dict(
            groupBy=["attribute1", "attribute4"],
            metrics=["metric1", "metric2"],
            granularity="daily",
            filters=["attribute2:42", "attribute3:42", "attribute5:B", "attribute6:true"],
            endDate=datetime(2000, 1, 3, 1, 59, 59),
        ),
        events=_common_events,
        analytics=[
            (42, "A", "2000-01-01T00:00:00", 2, 0.2),
            (42, "A", "2000-01-02T00:00:00", 2, 0.3),
            (42, "A", "2000-01-03T00:00:00", 1, 0.1),
        ],
    )
    only_required_args = dict(
        query=dict(
            groupBy=["attribute1"],
            metrics=["metric2"],
            granularity="hourly",
        ),
        events=[
            e(11, "2000-01-01T01:00:01", 0, 0.81, 42),
            e(12, "2000-01-01T01:00:02", 0, 0.19, 42),
            e(13, "2000-01-01T01:00:03", 0, 0.021, 42),
            e(14, "2000-01-01T01:00:04", 0, 3.0),
        ],
        analytics=[
            (42, "2000-01-01T01:00:00", 1.02),  # 0.001 is below out 2 decimal places threshold
            (None, "2000-01-01T01:00:00", 3.0),
        ],
    )

    for name, case in locals().items():
        if name.startswith("_"):
            continue
        yield name, case


def _make_analytics_response_json(shorthand_items: list[tuple], query: JsonDict) -> list[JsonDict]:
    response_item_keys = (*query["groupBy"], "date", *query["metrics"])
    return [dict(zip(response_item_keys, row)) for row in shorthand_items]


@pytest.mark.parametrize(
    "query, events, analytics",
    argvalues=(
        pytest.param(
            case["query"],
            case["events"],
            case["analytics"],
            id=name,
        )
        for name, case in _cases()
    ),
)
def test_e2e_cases(query, events, analytics, client, truncate_events_table):
    # noinspection PyTypeChecker
    expected_items = _make_analytics_response_json(analytics, query)

    for event in events:
        client.post("/event", json=event)
    analytics_response = client.get("/analytics/query", params=query)

    assert analytics_response.json() == expected_items
