from datetime import datetime

import pytest

from adtech_trial._app.logic import get_analytics_query_statement
from adtech_trial._app.schemas import FilterItem


@pytest.mark.parametrize(
    "kwargs",
    [
        pytest.param(
            dict(
                granularity="hourly",
                metrics={"metric1"},
            ),
            id="minimum_parameters",
        ),
        pytest.param(
            dict(
                granularity="daily",
                metrics={"metric1", "metric2"},
                group_by={"attribute1", "attribute2"},
            ),
            id="non_empty_group_by",
        ),
        pytest.param(
            dict(
                granularity="hourly",
                metrics={"metric1", "metric2"},
                attr_filters=[
                    FilterItem(attribute="attribute1", value="42"),
                    FilterItem(attribute="attribute6", value="false"),
                ],
            ),
            id="with_attribute_filters",
        ),
        pytest.param(
            dict(
                granularity="hourly",
                metrics={"metric1"},
                start_ts=datetime(2023, 1, 1, 0, 0, 0),
                end_ts=datetime(2023, 1, 2, 0, 0, 0),
            ),
            id="start_and_end_ts",
        ),
        pytest.param(
            dict(
                granularity="daily",
                metrics={"metric1"},
                group_by={"attribute1"},
                start_ts=datetime(2023, 1, 1, 0, 0, 0),
            ),
            id="only_start_ts",
        ),
        pytest.param(
            dict(
                granularity="hourly",
                metrics={"metric1"},
                attr_filters=[FilterItem(attribute="attribute6", value="true")],
                end_ts=datetime(2023, 1, 1, 0, 0, 0),
            ),
            id="only_end_ts",
        ),
    ],
)
def test_smoke_get_analytics_query_statement(kwargs, execute_query):
    # noinspection PyArgumentList
    query = get_analytics_query_statement(**kwargs)
    execute_query(query)
