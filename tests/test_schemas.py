from typing import cast

import pytest

from adtech_trial._app.schemas import EventAttribute, FilterItem


@pytest.mark.parametrize(
    "attribute, str_value, typed_value",
    [
        ("attribute1", "42", 42),
        ("attribute2", "42", 42),
        ("attribute3", "42", 42),
        ("attribute4", "foo", "foo"),
        ("attribute5", "foo", "foo"),
        ("attribute6", "true", True),
        ("attribute6", "false", False),
    ],
)
def test_filter_item_value_as_intended_type(attribute, str_value, typed_value):
    filter_item = FilterItem(attribute=cast(EventAttribute, attribute), value=str_value)
    assert filter_item.value_as_intended_type == typed_value
