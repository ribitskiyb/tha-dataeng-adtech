import pytest
from fastapi import status

from adtech_trial._app import logic
from adtech_trial._app.logic import CURRENCY_TO_INT_COEF
from tests.helpers import dummy_event_json


class TestCreateEvent:
    def test_metric2_is_saved_as_integer_cents(self, client, execute_query):
        currency_as_int = 12345
        payload = dummy_event_json(metric2=currency_as_int / CURRENCY_TO_INT_COEF)
        response = client.post("/event", json=payload)

        assert response.json()["metric2"] == payload["metric2"]

        (db_event,) = execute_query(
            f"DELETE FROM events WHERE external_id = {payload['id']} RETURNING *"
        )
        assert db_event["metric2"] == currency_as_int

    @pytest.mark.parametrize(
        "invalid_payload",
        [
            pytest.param({"id": 1}, id="missing_field"),
            pytest.param(
                dummy_event_json(event_date="Jan 1, 2000, 12 a.m."), id="wrong_field_type"
            ),
        ],
    )
    def test_reponds_with_405_on_invalid_payload(self, client, invalid_payload):
        response = client.post("/event", json=invalid_payload)
        assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED

    def test_reponds_with_200_on_success(self, client, truncate_events_table):
        valid_payload = dummy_event_json()
        response = client.post("/event", json=valid_payload)
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == valid_payload

    def test_prevents_saving_already_existing_external_id(self, client, truncate_events_table):
        payload = dummy_event_json()
        assert client.post("/event", json=payload).status_code == status.HTTP_200_OK
        assert client.post("/event", json=payload).status_code == status.HTTP_409_CONFLICT


class TestGetAnalytics:
    @pytest.mark.parametrize(
        "kv",
        [
            pytest.param("attribute1:not_a_number", id="invalid_value"),
            pytest.param("attr42:", id="invalid_attribute"),
            pytest.param("attribute1", id="invalid_format"),
        ],
    )
    def test_responds_with_405_on_invalid_filter_item(self, kv, client):
        query_params = {
            "granularity": "hourly",
            "groupBy": ["attribute1"],
            "metrics": ["metric1"],
            "filters": [kv, "attribute4:valid"],
        }
        response = client.get("/analytics/query", params=query_params)

        assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED

    def test_responds_with_403_when_too_many_result_rows(
        self,
        client,
        monkeypatch,
        truncate_events_table,
    ):
        client.post("/event", json=dummy_event_json())
        # noinspection PyUnresolvedReferences
        monkeypatch.setattr(logic, "ANALYTICS_RESULT_ROWS_THRESHOLD", 0)
        response = client.get(
            "/analytics/query",
            params={"granularity": "daily", "groupBy": ["attribute1"], "metrics": ["metric1"]},
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN
        assert response.json()["detail"].startswith(
            "Your request would produce 1 items in the response"
        )
